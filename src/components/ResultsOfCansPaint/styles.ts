import styled from 'styled-components';

export const Container = styled.div`
  margin-top: 32px;

  padding: 20px;
  background: linear-gradient(180deg, #fbfbfb 0%, #d9d9d9 100%);
  color: #484848;
  border: 1px solid #def1f4;
  border-radius: 8px;
  font-size: 18px;
  font-family: candara;

  ul {
    margin-top: 10px;
    li {
      margin-left: 30px;

      & + li {
        margin-top: 10px;
      }
    }
  }
`;
