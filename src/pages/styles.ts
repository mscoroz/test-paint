/* eslint-disable prettier/prettier */
import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  margin: 20px auto;
  padding: 20px 16px;
  background: linear-gradient(180deg, #fbfbfb 0%, #d9d9d9 100%);
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const Title = styled.h1`
  font-size: 32px;
  text-align: center;
  font-family: candara;
  text-shadow:
               -1px -1px 0px #FFF,
               -1px 1px 0px #FF7518,
                1px -1px 0px #FFF,
                1px 0px 0px #FFF;
`;

export const Text = styled.h1`
  font-size: 20px;
  text-align: center;
  font-family: candara;
`;

export const Form = styled.form`
  margin-top: 30px;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const WrapperCardWalls = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  gap: 16px;
  font-family: candara;
`;

export const Button = styled.button`
  margin-top: 30px;
  width: 100%;
  max-width: 150px;
  height: 40px;
  border: 0;
  background-color: #8B4000;
  border-radius: 8px;
  color: #fff;
  font-weight: bold;
  font-family: candara;
  display: flex;
  align-items: center;
  justify-content: center;

  transition: background-color 0.3s;

  &:hover {
    background-color: #B87333;
  }
`;
